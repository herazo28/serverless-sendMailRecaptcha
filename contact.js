import AWS from "aws-sdk";
var Request = require("request");

AWS.config.update({ region: "us-east-1" });

export function main(event, context, callback) {

    // Set response headers to enable CORS (Cross-Origin Resource Sharing)
    var headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
    };
    
    try {

        // Creates the Recaptcha validator URL
        var recaptcha_url = "https://www.google.com/recaptcha/api/siteverify?";
        recaptcha_url += "secret=" + '6LfVwFsUAAAAAPB9_sjrvxN9lTeuEF97XsD5l3JF' + "&";
        recaptcha_url += "response=" + event.recaptcha;

        Request(recaptcha_url, function(error, resp, body) {
            body = JSON.parse(body);

            if (error || !body.success) {
                // Return status code 400 on bad request
                var response1 = {
                    statusCode: 400,
                    headers: headers,
                    body: JSON.stringify({ status: false, message: "You appear to be a robot!" })
                };
                callback(null, response1);
                return;
            }
            else {
                // Set email parameters
                var eParams = {
                    Destination: {
                        ToAddresses: ["contact@poolervans.com"]
                    },
                    Message: {
                        Body: {
                            Text: {
                                Data: event.body
                            }
                        },
                        Subject: {
                            Data: event.subject
                        }
                    },
                    Source: "publicidad@poolervans.com"
                };

                // Instance of SES
                var ses = new AWS.SES({
                    region: 'us-east-1'
                });

                var email = ses.sendEmail(eParams, function(err, data) {
                    
                    if(err) {
                        // Return status code 500 on internal server error
                        var response2 = {
                            statusCode: 500,
                            headers: headers,
                            body: JSON.stringify({ 
                                status: false, 
                                message: "Internal server error",
                                error: err.message })
                        };
                        callback(null, response2);
                        return;
                    }
                    else {
                        // Return status code 200 on success
                        var response3 = {
                            statusCode: 200,
                            headers: headers,
                            body: JSON.stringify({ status: true, message: "Mail sent" })
                        };
                        callback(null, response3);
                    }
                    
                });
            }
        });
        
    }
    catch(e) {
        console.log(e);
        callback(null, failure({status: false}));
    }
}